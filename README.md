# Flask Weather App 


Deployed at: 

https://hefbupe.sga.dom.my.id/

This is a Flask (Python) consuming data from the openwather API to fetch current weather from passed city

![](static/img/home.png)
![](static/img/result.png)

_Example screenshot of flask_weather application_

## Setup
1. Install Dependencies
  - `pip install requirement.txt`
2. Run
  - `python app.py`


## Functionality

- Takes City Name as Input from the user.
- Use the api call to search the city name.
- Bring data as JSON file.
- Filter the JSON data and gives proper output. 




 
